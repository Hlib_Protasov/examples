# Explorer

### [LP-pancakeswap](https://gitlab.devlits.com/cryptolits/solidity/examples/-/tree/LP-pancakeswap)

---
Demonstration of how to create a liquidity pool at PancakeSwap via code.

### [Staking_pool](https://gitlab.devlits.com/cryptolits/solidity/examples/-/tree/staking_pool)

---
Demonstration of staking pool implementation with complicated computation.

### [Nft metadata](https://gitlab.devlits.com/cryptolits/solidity/examples/-/tree/create_metadata)

---
Demonstration and instruction of creating Nft collection with metadata.

### [Chainlink swap platform](https://gitlab.devlits.com/cryptolits/solidity/examples/-/tree/swap-with-chainlink)

---
Swap platform that uses chainlink's AggregatorV3 to provide price feeds.

### [ERC1155](https://gitlab.devlits.com/cryptolits/solidity/examples/-/tree/erc1155)

---
Realization of smart contract that mint Erc1155 tokens.
